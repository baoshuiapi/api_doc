# 报税 API 接口文档

⚠️ 当前站点不再维护，更多新文档支持，请移步到 [新的文档中心](https://baoshuiapi.com/doc/#/page/)

感谢大家的理解与支持，如有疑问可添加我们的客服，与我们联系

![客服二维码](https://baoshuiapi.com/media/images/ft-ewm1.jpg)


* [平台能力](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/%E5%B9%B3%E5%8F%B0%E8%83%BD%E5%8A%9B.md)
* [HTTP API说明](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/API%E8%AF%B4%E6%98%8E)
    * [HTTP接口规范](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/API%E8%AF%B4%E6%98%8E/%E6%8E%A5%E5%8F%A3%E8%A7%84%E8%8C%83.md)
    * [签名认证](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/API%E8%AF%B4%E6%98%8E/%E7%AD%BE%E5%90%8D%E9%AA%8C%E8%AF%81.md)
* [企业相关接口](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/%E4%BC%81%E4%B8%9A%E6%8E%A5%E5%8F%A3/)
    * [删除企业](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/%E4%BC%81%E4%B8%9A%E6%8E%A5%E5%8F%A3/%E5%88%A0%E9%99%A4%E4%BC%81%E4%B8%9A.md)
    * [更新企业信息](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/%E4%BC%81%E4%B8%9A%E6%8E%A5%E5%8F%A3/%E6%9B%B4%E6%96%B0%E4%BC%81%E4%B8%9A.md)
    * [获取公司列表](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/%E4%BC%81%E4%B8%9A%E6%8E%A5%E5%8F%A3/%E8%8E%B7%E5%8F%96%E5%85%AC%E5%8F%B8%E5%88%97%E8%A1%A8.md)
    * [查询企业信息](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/%E4%BC%81%E4%B8%9A%E6%8E%A5%E5%8F%A3/%E6%9F%A5%E8%AF%A2%E4%BC%81%E4%B8%9A%E4%BF%A1%E6%81%AF.md)
    * [查询企业税费种认定信息](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/%E4%BC%81%E4%B8%9A%E6%8E%A5%E5%8F%A3/%E6%9F%A5%E8%AF%A2%E4%BC%81%E4%B8%9A%E7%A8%8E%E8%B4%B9%E7%A7%8D%E8%AE%A4%E5%AE%9A%E4%BF%A1%E6%81%AF.md)
    * [查询需申报的税种信息](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/%E4%BC%81%E4%B8%9A%E6%8E%A5%E5%8F%A3/%E6%9F%A5%E8%AF%A2%E9%9C%80%E7%94%B3%E6%8A%A5%E7%A8%8E%E7%A7%8D%E4%BF%A1%E6%81%AF.md)
* [报税相关](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/%E6%8A%A5%E7%A8%8E%E6%8E%A5%E5%8F%A3/)
    * [创建任务v2（公测中）](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/%E6%8A%A5%E7%A8%8E%E6%8E%A5%E5%8F%A3/%E5%88%9B%E5%BB%BA%E4%BB%BB%E5%8A%A1_v2.md)
    * [任务状态回调v2（公测中）](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/%E6%8A%A5%E7%A8%8E%E6%8E%A5%E5%8F%A3/%E4%BB%BB%E5%8A%A1%E7%8A%B6%E6%80%81%E5%9B%9E%E8%B0%83_v2.md)
    * [获取任务状态v2（公测中）](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/%E6%8A%A5%E7%A8%8E%E6%8E%A5%E5%8F%A3/%E8%8E%B7%E5%8F%96%E4%BB%BB%E5%8A%A1%E7%8A%B6%E6%80%81_v2.md)
    * [中止任务](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/%E6%8A%A5%E7%A8%8E%E6%8E%A5%E5%8F%A3/%E5%81%9C%E6%AD%A2%E4%BB%BB%E5%8A%A1.md)
    * [获取工资薪金所得报表](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/%E6%8A%A5%E7%A8%8E%E6%8E%A5%E5%8F%A3/%E8%8E%B7%E5%8F%96%E5%B7%A5%E8%B5%84%E8%96%AA%E9%87%91%E6%89%80%E5%BE%97%E8%A1%A8.md)
    * [验证码上传](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/%E6%8A%A5%E7%A8%8E%E6%8E%A5%E5%8F%A3/%E9%AA%8C%E8%AF%81%E7%A0%81%E4%B8%8A%E4%BC%A0.md)
* [附录](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/%E9%99%84%E5%BD%95/)
    * [所属税局地区编码](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/%E9%99%84%E5%BD%95/%E6%89%80%E5%B1%9E%E7%A8%8E%E5%B1%80%E5%9C%B0%E5%8C%BA%E7%BC%96%E7%A0%81.md)
    * 报表代码对应表
        * [工资薪金所得申报表（个税）](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/%E9%99%84%E5%BD%95/%E5%B7%A5%E8%B5%84%E8%96%AA%E9%87%91%E6%89%80%E5%BE%97%E7%94%B3%E6%8A%A5%E8%A1%A8.md)
        * [增值税(小规模纳税人)](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/%E9%99%84%E5%BD%95/%E5%A2%9E%E5%80%BC%E7%A8%8E%EF%BC%88%E5%B0%8F%E8%A7%84%E6%A8%A1%E7%BA%B3%E7%A8%8E%E4%BA%BA%EF%BC%89.md)
        * [增值税(一般纳税人)](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/%E9%99%84%E5%BD%95/%E5%A2%9E%E5%80%BC%E7%A8%8E%EF%BC%88%E4%B8%80%E8%88%AC%E7%BA%B3%E7%A8%8E%E4%BA%BA%EF%BC%89)
            * [上海选填](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/%E9%99%84%E5%BD%95/%E5%A2%9E%E5%80%BC%E7%A8%8E%EF%BC%88%E4%B8%80%E8%88%AC%E7%BA%B3%E7%A8%8E%E4%BA%BA%EF%BC%89/%E4%B8%8A%E6%B5%B7%E9%80%89%E9%A2%98.md)
        * [企业所得税A类月(季)](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/%E9%99%84%E5%BD%95/%E4%BC%81%E4%B8%9A%E6%89%80%E5%BE%97%E7%A8%8EA%E7%B1%BB%E6%9C%88%EF%BC%88%E5%AD%A3%EF%BC%89.md)
        * [财务报表](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/%E9%99%84%E5%BD%95/%E8%B4%A2%E5%8A%A1%E6%8A%A5%E8%A1%A8.md)
* [v1](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/v1/)
    * [创建任务](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/v1/%E5%88%9B%E5%BB%BA%E4%BB%BB%E5%8A%A1.md)
    * [确认税额](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/v1/%E7%A1%AE%E8%AE%A4%E7%A8%8E%E9%A2%9D.md)
    * [申报数据上传](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/v1/%E7%94%B3%E6%8A%A5%E6%95%B0%E6%8D%AE%E4%B8%8A%E4%BC%A0.md)
    * [任务状态回调](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/v1/%E4%BB%BB%E5%8A%A1%E7%8A%B6%E6%80%81%E5%9B%9E%E8%B0%83.md)
    * [获取任务状态](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/v1/%E8%8E%B7%E5%8F%96%E4%BB%BB%E5%8A%A1%E7%8A%B6%E6%80%81.md)
    * [套餐介绍](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/v1/%E5%A5%97%E9%A4%90%E4%BB%8B%E7%BB%8D.md)
    * [套餐购买](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/v1/%E5%A5%97%E9%A4%90%E8%B4%AD%E4%B9%B0.md)
