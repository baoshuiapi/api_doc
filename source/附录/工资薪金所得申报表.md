报表代码: **gzxjsdsbb**

- 参考[人员信息申报](https://res.zijizhang.com/openTax/gzxj.xls)
- 参考[工资薪金申报](https://res.zijizhang.com/openTax/ryxx.xls)

#### 一、人员信息字段说明

> 假设当前申报的是 2 月份的个税，如果小明在 2 月份期间或者 2 月份之后离职，此员工需要申报 2 月份个税
> 假设当前申报的是 2 月份的个税，如果小明在 2 月份前（不含 2 月份）离职，申报 2 月份个税的时候，需要把小明的人员状态标记成非正常，写上离职日期，并把数据提交过来，此时，不需要传小明的工资薪金数据
> 本接口支持逾期申报，前提是当地的扣缴费端支持，我们也支持
> 注意：所有金额，请以元为单位，保留两位小数，**请去掉千分位分隔符**
> 如：一万二千三百四十五元，请输入：12345.00

参数名 | 数据类型 | 必填 | 描述
--- | --- | --- | ---
gh | string |  | 工号
xm | string | 必填  | 姓名，证件类型为外国护照、外国人永久居留身份证（外国人永久居留证）时，录入证件上的英文姓名，其他证件录入中文姓名。特殊符号，请使用「 · 」，如：艾散·亚森
zjlx | string | 必填 | 证件类型，填：居民身份证；中国护照；外国护照；港澳居民来往内地通行证；台湾居民来往大陆通行证
zjhm | string | 必填 | 证件号码，当 zjlx=居民身份证，请输入18位居民身份证号
gjdq | string | 必填 | 国籍(地区)
xb | string | 必填 | 性别，填：男；女
csrq | string | 必填 | 出生日期，格式：yyyy-mm-dd
ryzt | string |  | 人员状态，填：正常；非正常
rzsgcylx | string | 必填 | 任职受雇从业类型，填：雇员；其他
rzndjyqx | string |  rzsgcylx=雇员 ，且任职受雇从业日期大于等于2020年7月时可选 | 入职年度就业情形，填：当年首次入职学生；当年首次入职其他人员
sjhm | string | 当 rzsgcylx=雇员 时必填 | 手机号码，11位
rzsgcyrq | string | 当 rzsgcylx=雇员 时必填 | 任职受雇从业日期，格式：yyyy-mm-dd
lzrq | string | 当"人员状态"为"非正常"时必填 | 离职日期，格式：yyyy-mm-dd
sfcj | string |  | 是否残疾，填：是；否
sfls | string |  | 是否烈属，填：是；否
sfgl | string |  | 是否孤老，填：是；否
cjzh | string | 是否残疾为"是"时，残疾证号必填 | 残疾证号
lszh | string | 是否烈属为"是"时，烈属证号必填 | 烈属证号
sfkcjcfy | string |  | 是否扣除减除费用，填：是；否
grtze | string | 投资者的个人投资额和投资比例必须同时为空或同时大于0 | 个人投资额
grtzbl | string | 个人投资额不为空时，投资比例必须大于0小于等于100 | 个人投资比例(%)
bz | string |  | 备注
zwm | string |  | 中文名
sssy | string | zjlx != 居民身份证 时必填 | 涉税事由，填：任职受雇；提供临时劳务；转让财产；从事投资和经营活动；其他
csgjdq | string | zjlx != 居民身份证 时必填 | 出生国家(地区)
scrjsj | string | zjlx != 居民身份证，且 rzsgcylx=雇员 时必填 | 首次入境时间，格式：yyyy-mm-dd
yjljsj | string | zjlx != 居民身份证，且 rzsgcylx=雇员 时必填 | 预计离境时间，格式：yyyy-mm-dd
qtzjlx | string |  | 其他证件类型，详见 **其他证件类型对照表**
qtzjhm | string | 选择了其他证件类型时必填 | 其他证件号码
hjszdp | string |  | 户籍所在地（省）
hjszdc | string |  | 户籍所在地（市）
hjszdqx | string |  | 户籍所在地（区县）
hjszdjd | string |  | 户籍所在地（街道）
hjszdxxdz | string |  | 户籍所在地（详细地址）
jcjzdp | string |  | 经常居住地（省），详见 **省份表**
jcjzdc | string |  | 经常居住地（市）
jcjzdqx | string |  | 经常居住地（区县）
jcjzdjd | string |  | 经常居住地（街道）
jcjzdxxdz | string |  | 经常所在地（详细地址）
lxdzp | string |  | 联系地址（省），详见 **省份表**
lxdzc | string |  | 联系地址（市）
lxdzqx | string |  | 联系地址（区县）
lxdzjd | string |  | 联系地址（街道）
lxdzxxdz | string |  | 联系地址（详细地址）
dzyx | string |  | 电子邮箱
xl | string |  | 学历，填：研究生；大学本科；大学本科以下
khyx | string | "开户银行、银行账号、开户行省份"需要同时录入 | 开户银行
yxzh | string | "开户银行、银行账号、开户行省份"需要同时录入 | 银行账号
khxsf | string | "开户银行、银行账号、开户行省份"需要同时录入 | 开户行省份
zw | string |  | 职务，填：普通；高层

### 二、工资薪金字段说明

> 注意：所有金额，请以元为单位，保留两位小数，**请去掉千分位分隔符**
> 如：一万二千三百四十五元，请输入：12345.00

参数名 | 数据类型 | 必填 | 描述
--- | --- | --- | ---
gh | string |  | 工号
xm | string |  必填 | 姓名，特殊符号，请使用「 · 」，如：艾散·亚森
zzlx | string | 必填 | 证照类型，填：居民身份证；港澳居民来往内地通行证；港澳居民居住证；台湾居民来往大陆通行证；台湾居民居住证；中国护照；外国护照；外国人永久居留身份证；外国人工作许可证（A类）；外国人工作许可证（B类）；外国人工作许可证（C类）；其他个人证件
zzhm | string | 必填 | 证照号码
bqsr | float | 必填 | 本期收入（元）
bqmssr | float |  | 本期免税收入（元）
jbpbxf | float |  | 基本养老保险费（元）
jbmbxf | float |  | 基本医疗保险费（元）
sybxf | float |  | 失业保险费（元）
zfgjj | float |  | 住房公积金（元）
ljznjy | float |  | 累计子女教育（元）
ljjxjy | float |  | 累计继续教育（元）
ljzfdklx | float |  | 累计住房贷款利息（元）
ljzfzj | float |  | 累计住房租金（元）
ljsylr | float |  | 累计赡养老人（元）
qyzynj | float |  | 企业(职业)年金（元）
syjkbx | float |  | 商业健康保险（元）
syylbx | float |  | 税延养老保险（元）
qt | float |  | 其他（元）
zykcdjze | float |  | 准予扣除的捐赠额（元）
jmse | float |  | 减免税额（元）
bz | string | 当 qt>0 时必填 | 备注
qnycxjjsr_je | float | 申报 全年一次性奖金 的时候必填 | 金额（全年一次性奖金）
qnycxjjsr_mssr | float | | 免税收入（全年一次性奖金），须小于或等于 qnycxjjsr_je
qnycxjjsr_qt | float | | 其他（全年一次性奖金），须小于或等于 qnycxjjsr_je
qnycxjjsr_zykcdjze | float | | 准予扣除的捐赠额（全年一次性奖金），须小于或等于 qnycxjjsr_je
qnycxjjsr_jmse | float | | 减免税额（全年一次性奖金），须小于或等于 qnycxjjsr_je
qnycxjjsr_bz | string | 当 qnycxjjsr_qt>0 时必填 | 备注（全年一次性奖金）

### 例子

* v1 报表例子

```json
{
    "ryxx": [
        {
            "gh": "007",
            "xm": "张三",
            "zjlx": "居民身份证",
            "zjhm": "44xxxxxxxxxxxxxxx",
            "gjdq": "中国",
            "xb": "男",
            "ryzt": "正常",
            "csrq": "2021-01-01",
            "rzsgcylx": "雇员",
            "rzsgcyrq": "2021-01-01",
            "sjhm": "13xxxxxxxxx"
        }
    ],
    "gzxj": [
        {
            "gh": "007",
            "xm": "张三",
            "zzlx": "居民身份证",
            "zzhm": "44xxxxxxxxxxxxxxx",
            "bqsr": "0.00"
        }
    ]
}
```

* v2 接口请求例子

申报 2022年6月 的个税

```json
{
    "task_type":201,
    "tax":
    [
        {
            "scope":"gzxjsd",
            "tax_type":1,
            "time":{
                "term":1,
                "year":2022,
                "month":6
            },
            "report":[
                {
                    "report_name":"gzxjsdsbb",
                    "content":{
                        "ryxx":[
                            {
                                "xm":"小明",
                                "zjlx":"居民身份证",
                                "zjhm":"320321198507070123",
                                "gjdq":"中国",
                                "xb":"男",
                                "ryzt":"正常",
                                "csrq":"1985-07-07",
                                "rzsgcylx":"雇员",
                                "rzsgcyrq":"2017-06-13",
                                "sjhm":"11111111111"
                            }
                        ],
                        "gzxj":[
                            {
                                "xm":"小明",
                                "zzlx":"居民身份证",
                                "zzhm":"320321198507070123",
                                "bqsr":"3,300.00",
                                "zfgjj":"0.00",
                                "sybxf":"0.00",
                                "jbmbxf":"0.00",
                                "jbpbxf":"0.00",
                                "qt":"0.00",
                                "jmse":"0.00"
                            }
                        ]
                    }
                }
            ]
        }
    ]
}
```

确认税额 2022年6月的税额（如果税额>0的情况）

```json
{
    "task_type":201,
    "tax":
    [
        {
            "scope":"gzxjsd",
            "tax_type":3,
            "time":{
                "term":1,
                "year":2022,
                "month":6
            }
    ]
}
```

作废申报

```json
{
    "task_type":201,
    "tax":
    [
        {
            "scope":"gzxjsd",
            "tax_type":4,
            "time":{
                "term":1,
                "year":2022,
                "month":6
            }
    ]
}
```


### 四、附录

####   * 其他证件类型对照表

证件类型 | 其他证件类型
--- | ---
居民身份证 | 
中国护照 | 中国护照
港澳居民来往内地通行证 | 中华人民共和国港澳居民居住证
台湾居民来往大陆通行证 | 中华人民共和国台湾居民居住证
外国护照 | 外国人永久居留身份证（外国人永久居留证）；中华人民共和国外国人工作许可证（A类）；中华人民共和国外国人工作许可证（B类）；中华人民共和国外国人工作许可证（C类）；外国护照

#### * 省份表
省份 |
--- |
北京市 |
天津市 |
河北省 |
山西省 |
内蒙古自治区 |
辽宁省 |
吉林省 |
黑龙江省 |
上海市 |
江苏省 |
浙江省 |
安徽省 |
福建省 |
江西省 |
山东省 |
河南省 |
湖北省 |
湖南省 |
广东省 |
广西壮族自治区 |
海南省 |
重庆市 |
四川省 |
贵州省 |
云南省 |
西藏自治区 |
陕西省 |
甘肃省 |
青海省 |
宁夏回族自治区 |
新疆维吾尔自治区 |
台湾省 |
香港特别行政区 |
澳门特别行政区 |

