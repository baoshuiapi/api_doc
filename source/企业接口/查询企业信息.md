## 接口说明

> [GET] https://api.fpapi.net/v1/{aid}/companies/{company_id}

查询企业信息

## 请求参数

参数名 | 数据类型 | 参数类型 | 必填 | 描述
--- | --- | --- | --- | ---
aid | string | **path** | 必填 | 账号ID
company_id  | String | **path** | 必填 | 企业在平台对应的唯一标识

## 响应参数

参数 | 类型 | 描述
--- | --- | ---
code|int|200为正常，其他为错误代码
msg|string|返回信息
data|object|
data.company_id|string|企业在平台对应的唯一标识
data.company_name | string | 公司名称
data.tax_no | string | 纳税人识别号
data.tax_type | int | 公司名称
tax_area_code | int | body | 必填 | [地区编号](https://gitlab.com/baoshuiapi/api_doc/-/blob/main/source/%E9%99%84%E5%BD%95/%E6%89%80%E5%B1%9E%E7%A8%8E%E5%B1%80%E5%9C%B0%E5%8C%BA%E7%BC%96%E7%A0%81.md)
data.login_type | int | 登录方式 1:CA证书；2:用户名+密码；3:手机号+密码；4:证件号码+密码；5:手机号+验证码；6:密码 + 手机号+ 验证码
login_account | string | 登录账号
login_password | string | 登录密码
personal_identity | int | 个人身份登录类型 1:法定代表人、2:财务负责人、3:办税员
personal_account | string | 个人身份登录-账号
personal_password | string| 个人身份登录-密码
personal_name | string | 个人身份登录-姓名
personal_phone | string | 个人身份登录-手机号
personal_idcard | string | 个人身份登录-身份证
personal_tax_account  | string | 个税登录账号
personal_tax_password  | string |  个税登录密码
notify_url | string | 回调地址

## 返回例子

~~~
{
    "msg": "ok", 
    "code": 200, 
    "data": {
        "company_id": "99e6007ea09e0907df6f7d0f", 
        "tax_no": "123456789012345678", 
        "tax_area_code": "4400", 
        "company_name": "XXX有限公司", 
        "tax_type": 1, 
        "notify_url": "https://baidu.com", 
        "personal_tax_account": "18888888888", 
        "personal_tax_password": "pwd", 
        "login_type": 1, 
        "login_account": "13302533659", 
        "login_password": "password", 
        "personal_identity": 3, 
        "personal_account": "", 
        "personal_password": "", 
        "personal_name": "张三", 
        "personal_phone": "18888888888", 
        "personal_idcard": "111111111111111111"
    }
}
~~~

