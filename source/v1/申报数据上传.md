## 接口说明
`[POST] https://api.fpapi.net/v1/{aid}/companies/{company_id}/report`

该接口用于上报申报数据到平台，创建申报任务前需要上报报税所需要的申报报表

## 请求参数
参数名 | 数据类型 | 参数类型 | 必填 | 描述
--- | --- | --- | --- | ---
aid | string | path | 必填 | 账号ID
company_id | string | path | 必填 | 公司ID
report_list| array | body | 必填 | 报表列表
report_list.report_name| string | body | 必填 | 请填写报表代码对应表的[报表代码](报表代码对应表.md)
report_list.report_content| object | body | 必填 | 请填写报表代码对应表的[报表结构](报表代码对应表.md)
report_list.time | object | body | 必填 | 报表所属期间
report_list.time.term | string | body | 必填 | 报表期间类型 1: 月 2: 季 3: 年
report_list.time.year | int | body | 必填 | 年份
report_list.time.quarter | string | body |  | 报表期间类型选择季度必填
report_list.time.month | string | body |  | 报表期间类型选择月度必填

## 请求例子

> 工资薪金所得申报表（个税申报）

```
{
	"report_list": [{
		"report_name": "gzxjsdsbb",
		"time": {
			"term": 1,
			"year": 2022,
			"month": 1
		},
		"report_content": {
			"ryxx": [{
				"gh": "007",
				"xm": "张三",
				"zjlx": "居民身份证",
				"zjhm": "44xxxxxxxxxxxxxxx",
				"gjdq": "中国",
				"xb": "男",
				"ryzt": "正常",
				"csrq": "2021-01-01",
				"rzsgcylx": "雇员",
				"rzsgcyrq": "2021-01-01",
				"sjhm": "13xxxxxxxxx"
			}],
			"gzxj": [{
				"gh": "007",
				"xm": "张三",
				"zzlx": "居民身份证",
				"zzhm": "44xxxxxxxxxxxxxxx",
				"bqsr": "0.00"
			}]
		}
	}]
}
```

## 响应参数
参数 | 类型 | 描述
--- | --- | ---
code|int|200为正常，其他为错误代码
msg|string|返回信息
data|object|

