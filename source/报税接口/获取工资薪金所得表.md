`[GET] https://api.fpapi.net/v1/{aid}/companies/{company_id}/report/gzxjsd/{month}`

获取工资薪金所得报表，仅在接口申报成功后，才可以查询

## 请求参数
参数名 | 数据类型 | 参数类型 | 必填 | 描述
--- | --- | --- | --- | ---
aid | string | **path** | 必填 | 账号ID
company_id | string | **path** | 必填 | 公司ID
month | string | **path** | 必填 | 所属月份，格式 "YYYY-MM"，如 2022-01

## 响应参数
参数 | 类型 | 描述
--- | --- | ---
code|int|200为正常，其他为错误代码
msg|string|返回信息
data|array|
data.month| int | 当月1日凌晨时间戳（秒级）10位
data.xm | string | 姓名
data.zjlx | string | 证照类型
data.zjhm | string | 证照号码
data.skssqq | string | 税款所属期起，如：2021-11-01
data.skssqz | string | 税款所属期止，如：2021-11-30
data.sdxm | string | 所得项目，如：正常工资薪金
data.bqsr | float | 本期收入(元)
data.bqfy | float | 本期费用(元)
data.bqmssr | float | 本期免税收入(元)
data.bqjbpbxf | float | 本期基本养老保险费(元)
data.bqjbmbxf | float | 本期基本医疗保险费(元)
data.bqsybxf | float | 本期失业保险费(元)
data.bqzfgjj | float | 本期住房公积金(元)
data.bqqyzynj | float | 本期企业(职业)年金(元)
data.bqsyjkbxf | float | 本期商业健康保险费(元)
data.bqsyylbxf | float | 本期税延养老保险费(元)
data.bqqtkcqt | float | 本期其他扣除(其他)(元)
data.ljsre | float | 累计收入额(元)
data.ljmssr | float | 累计免税收入(元)
data.ljjcfy | float | 累计减除费用(元)
data.ljzxkc | float | 累计专项扣除(元)
data.ljznjyzckc | float | 累计子女教育支出扣除(元)
data.ljjxjyzckc | float | 累计继续教育支出扣除(元)
data.ljzfdklxzckc | float | 累计住房贷款利息支出扣除(元)
data.ljzfzjzckc | float | 累计住房租金支出扣除(元)
data.ljsylrzckc | float | 累计赡养老人支出扣除(元)
data.ljqtkc | float | 累计其他扣除(元)
data.ljzykcdjz | float | 累计准予扣除的捐赠(元)
data.ljynssde | float | 累计应纳税所得额(元)
data.sl | string | 税率
data.sskcs | float | 速算扣除数(元)
data.ljynse | float | 累计应纳税额(元)
data.ljjmse | float | 累计减免税额(元)
data.ljykjse | float | 累计应扣缴税额(元)
data.ljyyjse | float | 累计已预缴税额(元)
data.ljybtse | float | 累计应补(退)税额(元)
data.bz | string | 备注
data.ljssyxyyezhkc | float | 3岁以下婴幼儿照护扣除(元)
data.jajsbl | string | 减按计税比例
data.ccyz | float | 财产原值
data.yxkcdsf | float | 允许扣除的税费(元)
data.jcfy | float | 减除费用(元)


## 返回例子

```
{
  "msg": "ok",
  "code": 200,
  "data": [
    {
      "month": 1633017600,
      "gh": "672fa9",
      "xm": "姓名",
      "zjlx": "居民身份证",
      "zjhm": "123456789012345678",
      "skssqq": "2021-11-01",
      "skssqz": "2021-11-30",
      "sdxm": "正常工资薪金",
      "bqsr": 0,
      "bqfy": 0,
      "bqmssr": 0,
      "bqjbpbxf": 0,
      "bqjbmbxf": 0,
      "bqsybxf": 0,
      "bqzfgjj": 0,
      "bqqyzynj": 0,
      "bqsyjkbxf": 0,
      "bqsyylbxf": 0,
      "bqqtkcqt": 0,
      "ljsre": 0,
      "ljmssr": 0,
      "ljjcfy": 55000,
      "ljzxkc": 0,
      "ljznjyzckc": 0,
      "ljjxjyzckc": 0,
      "ljzfdklxzckc": 0,
      "ljzfzjzckc": 0,
      "ljsylrzckc": 0,
      "ljqtkc": 0,
      "ljzykcdjz": 0,
      "ljynssde": 0,
      "sl": "0.03",
      "sskcs": 0,
      "ljynse": 0,
      "ljjmse": 0,
      "ljykjse": 0,
      "ljyyjse": 0,
      "ljybtse": 0,
      "bz": ""
    }
  ]
}
```

